import React from 'react'

import './global.css'
//sempre procura pelo arquivo index, por isso nao precisa colocar ele na rota
import Routes from './routes'

function App() {
  return (
    <Routes />
  )
}

export default App;
