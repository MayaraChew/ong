import React from 'react';
//funcao recebendo a props (propriedade)
//acessando o title pela props dentro de chaves em uma tag html
export default function Header({children}) {
    return(
        <header>
            <h1>{children}</h1>
        </header>
    )
}
