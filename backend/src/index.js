const express = require('express')
const cors = require('cors')
const routes = require('./routes')
const app = express()

app.use(cors())

//necessario inserir a url quando a aplicacao for disponibilizada aos usuarios
// app.use(cors({
//     origin: 'http://meuapp.com'
// })

app.use(express.json())
app.use(routes)

app.listen(3333)